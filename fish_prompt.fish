
# SEGMENTS

function mcgp_start_segment
	set -l left_segment_separator \uE0B6
	set -l segment_conector \ue0bc

	set -l bg $argv[1]
	set -e argv[1]
	set -l fg $argv[1]
	set -e argv[1]

	set_color normal

	switch "$_bg_color"
		case 'null'
			set_color -b $bg
			echo -ns ' '
		case "$bg"
			set_color -b $bg
			echo -ns ' '
			#NO OP
		case ''
			set_color $bg
			echo -ns $left_segment_separator
		case '*'
			set_color -b $_bg_color
			echo -ns ' '
			set_color -b $bg $_bg_color
			echo -ns $segment_conector ' '
	end

	set_color -b $bg $fg $argv

	set _bg_color "$bg"
end

function mcgp_end_segment
	set -l right_segment_separator \uE0B4

	if [ -z "$_bg_color" ];
		return
	end

	if [ "$_bg_color" = "null" ]
		set _bg_color ''
		return
	end

	set_color normal
	set_color $_bg_color
	echo -ns $right_segment_separator
	set_color normal
	set -g _bg_color
end

# PATH HELPERS

function basename
	string replace -r '^.*/' '' -- $argv
end

function dirname
	string replace -r '/[^/]+/?$' '' -- $argv
end

function mcgp_short_path -a path
	set -l shortened_dir_length 1

	set -l home_regex (string escape --style=regex "$HOME")

	set -l home_compressed (string replace -r "^$home_regex" "~" $path)

	string replace -ar '(\.?[^/]{'"$shortened_dir_length"'})[^/]*/' '$1/' "$home_compressed"
end

function mcgp_path_prompt -a path
	set -l path_font $THEME_DEF
	set -l current_dir_font --bold

	set -l compressed (mcgp_short_path $path)
	set_color $path_font
	echo -n (string replace -r '[^/]*$' '' $compressed)
	set_color $current_dir_font
	echo -n (string match -r '[^/]*$' $compressed)
	set_color normal
end

# GIT HELPERS

# 0 -> branch
# 1 -> tag
# 1 -> commit
# 3 -> fatal error
function mcgp_git_commitish
	set -l ref (command git symbolic-ref HEAD 2> /dev/null)
	and string replace -r '^refs/heads/' '' $ref
	and return 0


	set -l tag (command git describe --tags --exact-match 2> /dev/null)
	and echo $tag
	and return 1

	set -l commit (command git show-ref --head -s --abbrev ^HEAD 2> /dev/null)
	and echo $commit | head -n1
	and return 2

	return 3
end

function mcgp_git_commitish_pretty
	set -l branch_glyph \ue725
	set -l tag_glyph \U000f04f9
	set -l detached_glyph \ueafc

	set -l commitish (mcgp_git_commitish)

	switch "$status"
		case '0'
			echo -n "$branch_glyph "
		case '1'
			echo -n "$tag_glyph "
		case '2'
			echo -n "$detached_glyph "
	end

	echo $commitish
end

function mcgp_git_dir
	command git rev-parse --path-format=absolute --git-common-dir 2> /dev/null
	and return 0
	or return 1
end

function mcgp_inside_work_tree
	set -l worktree_dir (command git rev-parse --path-format=absolute --show-toplevel 2> /dev/null)
	or return 1

	set -l d $PWD
	set -l ret
	while string match -r $worktree_dir (realpath $d) 2>&1 > /dev/null
		set ret $d
		set d (dirname $d)
	end
	echo $ret
	return 0
end

function mcgp_inside_git_dir
	set -l git_dir (mcgp_git_dir)
	or return 1


	set -l d $PWD
	while [ "$d" ]
		if [ (realpath $d | string collect) = "$git_dir" ]
			echo $d
			return 0
		end

		if [ "$d" = '/' ]
			return 1
		end


		set d (dirname $d)
	end

	return 1
end

function mcgp_git_status
	set -l git_dirty_glyph \*
	set -l git_staged_glyph \&
	set -l git_ahead_glyph +
	set -l git_behind_glyph \-
	set -l git_untracked_glyph …

	set -l dirty ''
	set -l dirty (command git diff --no-ext-diff --quiet --exit-code 2>&1 > /dev/null; or echo -n "$git_dirty_glyph")

	set -l staged (command git diff --cached --no-ext-diff --quiet --exit-code 2> /dev/null || echo -n "$git_staged_glyph")

	set -l commits (command git rev-list --left-right '@{upstream}...HEAD' 2> /dev/null)
	set -l ahead (count (string match -r '^>' $commits))
	if [ $ahead -eq 0 ]
		set ahead ''
	else
		set ahead "$git_ahead_glyph$ahead"
	end

	set -l behind (count (string match -r '^<' $commits))
	if [ $behind -eq 0 ]
		set behind ''
	else
		set behind "$git_behind_glyph$behind"
	end

	[ (command git ls-files --other --exclude-standard --directory --no-empty-directory | string collect 2> /dev/null) ]
	and set -l new "$git_untracked_glyph"

	echo -ns (mcgp_git_commitish_pretty)

	set -l flags "$dirty$staged$ahead$behind$new"
	[ "$flags" ]
	and echo -ns ' ' "$flags"


	[ "$staged" ]
	and return 2

	[ "$dirty" ]
	and return 1

	return 0
end

# PROMPTS

function mcgp_mode_prompt
	set -l normal_font $THEME_PRI $THEME_BG --bold
	set -l visual_font $THEME_SEC $THEME_BG --bold
	set -l replace_font $THEME_TER $THEME_BG --bold
	if test "$fish_key_bindings" = fish_vi_key_bindings
		or test "$fish_key_bindings" = fish_hybrid_key_bindings

		switch $fish_bind_mode
			case default
				mcgp_start_segment $normal_font
				echo -n N
			case replace_one
				mcgp_start_segment $replace_font
				echo -n R
			case replace
				mcgp_start_segment $replace_font
				echo -n R
			case visual
				mcgp_start_segment $visual_font
				echo -n V
		end
	end
end

function mcgp_user_prompt
	set -l user_font $THEME_PRI $THEME_BG --bold

	if not set -q show_user_hostname && not set -q SSH_CLIENT
		return
	end

	mcgp_start_segment $user_font
	echo -ns (whoami) '@' (uname -n)
end

function mcgp_pwd_prompt
	echo -n ' '
	mcgp_path_prompt "$PWD"
	echo -n ' '
end

function mcgp_git_prompt
	set -l git_dir_font $THEME_PRI $THEME_BG --bold
	set -l git_clean_font $THEME_TER $THEME_BG
	set -l git_dirty_font $THEME_ALERT $THEME_DEF
	set -l git_staged_font $THEME_SEC $THEME_BG

	set -l git_dir (mcgp_git_dir)
	or begin
		mcgp_pwd_prompt
		and return
	end

	echo -n ' '

	if set -l parrent_git_dir (mcgp_inside_git_dir)
		echo -ns (mcgp_short_path (dirname $parrent_git_dir)/)

		mcgp_start_segment $git_dir_font
		echo -ns (basename $parrent_git_dir)

		set -l parrent_worktree (mcgp_inside_work_tree)
		if [ $status -eq 1 ]
			mcgp_end_segment
			mcgp_path_prompt (string replace -r $parrent_git_dir '' $PWD)
			echo -n ' '
			return
		end
	end

	set -q parrent_worktree || set -l parrent_worktree (mcgp_inside_work_tree)
	set -l commitish (mcgp_git_commitish)

	set -l git_status (mcgp_git_status)
	switch "$status"
		case '0'
			set git_font $git_clean_font
		case '1'
			set git_font $git_dirty_font
		case '2'
			set git_font $git_staged_font
	end

	if [ ! "$parrent_worktree" = "$parrent_git_dir/$commitish" ]
		mcgp_end_segment
	else
		set parrent_git_dir "$parrent_git_dir/"
	end

	echo -n (mcgp_short_path (string replace -ra "^$parrent_git_dir|"(echo "$commitish" | string collect)"\$" '' "$parrent_worktree"))

	if not [ (basename $parrent_worktree | string collect) = "$commitish" ]
		echo -n ' '
	end

	mcgp_start_segment $git_font
	echo -n $git_status
	mcgp_end_segment
	mcgp_path_prompt (string replace -r "^$parrent_worktree" '' "$PWD")
	echo -n ' '

end

function mcgp_RO_prompt
	set -l RO_font $THEME_ALERT $THEME_DEF --bold
	set -l RO_glyph \uf00d

	[ -w $PWD ]
	and return


	mcgp_start_segment $RO_font
	echo -ns $RO_glyph
end

function mcgp_job_prompt
	set -l job_font $THEME_DEF $THEME_PRI --bold
	set -l job_glyph \uf295

	[ (jobs -p | wc -l | string collect) -eq 0 ]
	and return

	mcgp_start_segment $job_font
	echo -ns $job_glyph
end

function mcgp_exit_code_prompt
	set -l exit_code_font $THEME_ALERT $THEME_DEF --bold

	set -l exit_code_glyph !

	[ "$last_status" -eq 0 ]
	and return

	mcgp_start_segment $exit_code_font
	echo -ns "$exit_code_glyph"
	[ "$last_status" -eq 1 ]
	or echo -ns " $last_status"
end

function fish_prompt
	set -g last_status $status

	set -g _bg_color 'null'
	mcgp_mode_prompt
	mcgp_RO_prompt
	mcgp_job_prompt
	mcgp_exit_code_prompt
	mcgp_user_prompt
	mcgp_end_segment
	mcgp_git_prompt

	set_color normal
end
